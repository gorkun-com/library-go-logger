package logger

import (
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/pkgerrors"
)

const (
	envLocal = "local"
	envProd  = "production"
)

type Fields = map[string]any

type Logger interface {
	Info(message string, fields Fields)
	Warn(message string, fields Fields)
	Error(message string, err error, fields Fields)
	Fatal(message string, err error, fields Fields)
}

type Options struct {
	Env         string
	ServiceName string
	TgBotKey    string
	TgChatID    int64
	TgThreadID  *int64
}

type zeroLogger struct {
	log zerolog.Logger
}

func (logger *zeroLogger) Debug(msg string, fields Fields) {
	logger.log.Debug().Fields(fields).Msg(msg)
}

func (logger *zeroLogger) Info(msg string, fields Fields) {
	logger.log.Info().Fields(fields).Msg(msg)
}

func (logger *zeroLogger) Warn(msg string, fields Fields) {
	logger.log.Warn().Fields(fields).Msg(msg)
}

func (logger *zeroLogger) Error(msg string, err error, fields Fields) {
	logger.log.Error().Err(err).Fields(fields).Msg(msg)
}

func (logger *zeroLogger) Fatal(msg string, err error, fields Fields) {
	logger.log.Fatal().Err(err).Fields(fields).Msg(msg)
}

func New(options Options) Logger {

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack

	if options.Env == envProd {

		hook := &telegramHook{
			label:    options.ServiceName,
			key:      options.TgBotKey,
			chatID:   options.TgChatID,
			threadID: options.TgThreadID,
		}

		log := zerolog.New(os.Stderr).Hook(hook).Level(zerolog.InfoLevel).With().Timestamp().Logger()

		return &zeroLogger{log: log}
	}

	if options.Env == envLocal {
		log := zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).Level(zerolog.DebugLevel).With().Stack().Timestamp().Logger()

		return &zeroLogger{log: log}
	}

	log := zerolog.New(os.Stdout).Level(zerolog.DebugLevel).With().Stack().Timestamp().Logger()

	return &zeroLogger{log: log}
}
