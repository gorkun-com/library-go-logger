package logger

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/rs/zerolog"
)

const telegramApi = "https://api.telegram.org/bot%s/sendMessage"
const messageTemplate = "[%s]: %s"

type telegramHook struct {
	label    string
	key      string
	chatID   int64
	threadID *int64
}

func (tg *telegramHook) Run(event *zerolog.Event, level zerolog.Level, message string) {

	if level < zerolog.ErrorLevel {
		return
	}

	if tg.key == "" || tg.chatID == 0 {
		return
	}

	url := fmt.Sprintf(telegramApi, tg.key)
	text := fmt.Sprintf(messageTemplate, tg.label, message)

	body := Fields{"chat_id": tg.chatID, "text": text}
	if tg.threadID != nil {
		body["message_thread_id"] = *tg.threadID
	}

	request, err := json.Marshal(body)
	if err != nil {
		event.AnErr("tg", err)
		return
	}

	response, err := http.Post(url, "application/json", bytes.NewBuffer(request))
	if err != nil {
		event.AnErr("tg", err)
		return
	}

	defer response.Body.Close()

	if response.StatusCode < http.StatusOK || response.StatusCode >= http.StatusBadRequest {
		event.AnErr("tg", err)
		return
	}

}
